<?php
/**
 * Functions File for Staff CPT
 *
 * @package  		ChurchAmp_Staff
 * @subpackage  	Includes
 * @version  		5.0.0
 * @since   		1.0.0
 * @author  		Endeavr Media <support@endeavr.com>
 * @copyright  	Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link   		http://churchamp.com/plugins/staff
 * @license  		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * @example		https://github.com/justintadlock/custom-content-portfolio/blob/master/includes/functions.php
 */

/**
 * Filter to ensure the proper labels display when user updates custom post type entries.
 * translators: %s: date and time of the revision
 * translators: Publish box date format, @example: http://php.net/date
 *
 * @since  0.1.0
 * @access public
 * @param  string $title
 * @return string
 */
add_filter( 'post_updated_messages', 'endvr_updated_messages_staff' );
function endvr_updated_messages_staff( $messages ) {
global $post, $post_ID;
	$messages['staff'] = array(
		0  => '', // Unused. Messages start at index 1.
		1  => sprintf( __('Staff Member Updated. <a href="%s">View Staff Member</a>'), esc_url( get_permalink($post_ID) ) ),
		2  => __('Staff Member Updated.'),
		3  => __('Staff Member Deleted.'),
		4  => __('Staff Member Updated.'),
		5  => isset($_GET['revision']) ? sprintf( __('Staff Member Restored to Revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6  => sprintf( __('Staff Member Published. <a href="%s">View Staff Member</a>'), esc_url( get_permalink($post_ID) ) ),
		7  => __('Staff Member Saved.'),
		8  => sprintf( __('Staff Member Submitted. <a target="_blank" href="%s">Preview Staff Member</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		9  => sprintf( __('Staff Member Scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Staff Member</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
		10  => sprintf( __('Staff Member Draft Updated. <a target="_blank" href="%s">Preview Staff Member</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);
	return $messages;
}

/**
 * Filter on 'post_type_archive_title' to allow for the use of the 'archive_title' label.
 * Not supported by WordPress natively, but that's okay since we can roll our own labels.
 *
 * @since  0.1.0
 * @access public
 * @param  string $title
 * @return string
 */

add_filter( 'post_type_archive_title', 'endvr_post_type_archive_title_staff' );
function endvr_post_type_archive_title_staff( $title ) {
	if ( is_post_type_archive( 'staff' ) ) {
		$post_type = get_post_type_object( 'staff' );
		$title = isset( $post_type->labels->archive_title ) ? $post_type->labels->archive_title : $title;
	}
	return $title;
}

/**
 * Filter to assign custom text instructions on publish form to replace "Enter title here".
 *
 * @since  0.1.0
 * @access public
 * @param  string $title
 * @return string
 */

add_filter( 'enter_title_here', 'endvr_replace_title_label_staff' );
function endvr_replace_title_label_staff( $title ) {
	$screen = get_current_screen();
	if  ( 'staff' == $screen->post_type ) {
		$title = 'Enter Staff Member Name Here (Last, First)...';
		return $title;
	}
}

/**
 * Assign a custom label to the Sub Title.
 *
 * @since  0.1.0
 * @access public
 * @example: http://codex.wordpress.org/Function_Reference/remove_filter
 */

add_action( 'admin_head', 'endvr_replace_subtitle_label_staff' );
function endvr_subtitle_label_staff() {
	echo 'Input a Subtitle for this Staff Member (Optional)';
}
function endvr_replace_subtitle_label_staff() {
	$screen = get_current_screen();
	if  ( 'staff' == $screen->post_type ) {
		remove_filter( 'endvr_subtitle_label', 'endvr_subtitle_label_default' );
		add_filter( 'endvr_subtitle_label', 'endvr_subtitle_label_staff' );
	}
}

/**
 * Assign a custom label to the Post Editor + place it in a sortable Meta Box.
 *
 * @since  0.1.0
 * @access public
 * @example: http://wordpress.org/support/topic/move-custom-meta-box-above-editor?replies=17
 * @example: http://software.troydesign.it/php/wordpress/move-wp-visual-editor.html
 */

add_action( 'add_meta_boxes', 'endvr_add_meta_box_editor_staff', 0 );
function endvr_add_meta_box_editor_staff() {
	$screen = get_current_screen();
	if  ( 'staff' == $screen->post_type ) {
		global $_wp_post_type_features;
		foreach ($_wp_post_type_features as $type => &$features) {
			if (isset($features['editor']) && $features['editor']) {
				unset($features['editor']);
				add_meta_box(
					'endvr_editor_description_staff',
					__('Staff Member Profile'),
					'endvr_meta_box_editor_staff',
					$type, 'normal', 'core'
				);
			}
		}
	}
	add_action( 'admin_head', 'endvr_admin_head_staff' ); //white background
}
function endvr_admin_head_staff() {
?>
	<style type="text/css">
		.wp-editor-container{background-color:#fff;}
	</style>
<?php
}
function endvr_meta_box_editor_staff( $post ) {
	echo '<div class="wp-editor-wrap">';
	wp_editor($post->post_content, 'content', array('dfw' => true, 'tabindex' => 1) );
	echo '</div>';
}

/**
 * Redefine the way the content type's admin panel index listing is displayed.
 *
 * @since  0.1.0
 * @access public
 * @example: http://codex.wordpress.org/Plugin_API/Action_Reference/manage_posts_custom_column
 * @example: http://thinkvitamin.com/code/create-your-first-wordpress-custom-post-type/
 */

add_filter( 'manage_edit-staff_columns', 'endvr_set_custom_edit_columns_staff' );
add_action( 'manage_staff_posts_custom_column',  'endvr_custom_column_staff', 10, 2 );

function endvr_set_custom_edit_columns_staff($columns) {
	// unset($columns['date']); // we want to show the date for staff
	return $columns
		+ array(
			'_endvr_staff_role' 		=> __('Position'),
			'_endvr_staff_phone' 		=> __('Phone'),
			'_endvr_staff_email'		=> __('Email'),
			'_endvr_staff_photo_thumb' 	=> __('Thumbnail'),
			'_endvr_staff_photo_full' 	=> __('Full Photo'),
	);
}

function endvr_custom_column_staff($column, $post_id) {
	switch ( $column ) {
		case '_endvr_staff_role':
			echo get_post_meta( $post_id , '_endvr_staff_role' , true );
		break;
		case '_endvr_staff_phone':
			echo get_post_meta( $post_id , '_endvr_staff_phone' , true );
		break;
		case '_endvr_staff_email':
			echo get_post_meta( $post_id , '_endvr_staff_email' , true );
		break;
		case '_endvr_staff_photo_thumb': ?>
			<img src="<?php echo get_post_meta( $post_id , '_endvr_staff_photo_thumb' , true ); ?>" width="133">
		<?php
		break;
		case '_endvr_staff_photo_full': ?>
			<img src="<?php echo get_post_meta( $post_id , '_endvr_staff_photo_full' , true ); ?>"width="100">
		<?php
		break;
	}
}