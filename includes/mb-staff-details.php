<?php
/**
 * Meta Boxes ( Register Field Groups for Staff CPT )
 *
 * @package    	ChurchAmp_Staff
 * @subpackage   	Includes
 * @version    	5.0.0
 * @since     		1.0.0
 * @author    		Endeavr Media <support@endeavr.com>
 * @copyright   	Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link     		http://churchamp.com/plugins/staff
 * @license    	http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * The Meta Boxes require the Advanced Custom Fields plugin be activated.
 *
 * The register_field_group function accepts 1 array which holds the relevant data to register a field group
 * You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
 * This code must run every time the functions.php file is read
 */

if(function_exists("register_field_group"))
{
	register_field_group(array (
			'id' => 'acf-staff-details',
			'title' => 'Staff Details',
			'fields' => array (
				0 => array (
					'key' => '_endvr_staff_role',
					'label' => 'Position/Role',
					'name' => '_endvr_staff_role',
					'type' => 'text',
					'order_no' => 0,
					'instructions' => 'Primary responsibility with the church (i.e. Senior Pastor).',
					'required' => 0,
					'conditional_logic' => array (
						'status' => 0,
						'rules' => array (
							0 => array (
								'field' => 'null',
								'operator' => '==',
								'value' => '',
							),
						),
						'allorany' => 'all',
					),
					'default_value' => '',
					'formatting' => 'none',
				),
				1 => array (
					'key' => '_endvr_staff_phone',
					'label' => 'Phone',
					'name' => '_endvr_staff_phone',
					'type' => 'text',
					'order_no' => 1,
					'instructions' => 'Primary phone number (format => XXX-XXX-XXXX).',
					'required' => 0,
					'conditional_logic' => array (
						'status' => 0,
						'rules' => array (
							0 => array (
								'field' => 'null',
								'operator' => '==',
								'value' => '',
							),
						),
						'allorany' => 'all',
					),
					'default_value' => '',
					'formatting' => 'none',
				),
				2 => array (
					'key' => '_endvr_staff_email',
					'label' => 'Email',
					'name' => '_endvr_staff_email',
					'type' => 'text',
					'order_no' => 2,
					'instructions' => 'Primary email address (format => name@churchdomain.org).',
					'required' => 0,
					'conditional_logic' => array (
						'status' => 0,
						'rules' => array (
							0 => array (
								'field' => 'null',
								'operator' => '==',
								'value' => '',
							),
						),
						'allorany' => 'all',
					),
					'default_value' => '',
					'formatting' => 'none',
				),
			),
			'location' => array (
				'rules' => array (
					0 => array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'staff',
						'order_no' => 1,
					),
				),
				'allorany' => 'all',
			),
			'options' => array (
				'position' => 'advanced',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 1,
		));
}