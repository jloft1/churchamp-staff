<?php
/**
 * Taxonomy ( Register Staff Role )
 *
 * @package  		ChurchAmp_Staff
 * @subpackage  	Includes
 * @version  		5.0.0
 * @since   		1.0.0
 * @author  		Endeavr Media <support@endeavr.com>
 * @copyright  	Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link   		http://churchamp.com/plugins/staff
 * @license  		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/* register and define the taxonomy on the 'init' hook */
/* @example: http://codex.wordpress.org/Function_Reference/register_taxonomy */
add_action( 'init', 'endvr_register_tax_staffrole' );
function endvr_register_tax_staffrole() {

	$labels = array(
		'name'                       	=> __( 'Staff Roles',                           		'churchamp-staff' ),
		'singular_name'              	=> __( 'Staff Role',                            		'churchamp-staff' ),
		'menu_name'                  	=> __( 'Staff Roles',                           		'churchamp-staff' ),
		'name_admin_bar'             	=> __( 'Staff Roles',                            		'churchamp-staff' ),
		'search_items'               	=> __( 'Search '.'Staff Roles'.'',                    	'churchamp-staff' ),
		'popular_items'              	=> __( 'Popular '.'Staff Roles'.'',                   	'churchamp-staff' ),
		'all_items'                  	=> __( 'All '.'Staff Roles'.'',                       	'churchamp-staff' ),
		'edit_item'                  	=> __( 'Edit '.'Staff Role'.'',                       	'churchamp-staff' ),
		'view_item'                  	=> __( 'View '.'Staff Role'.'',                       	'churchamp-staff' ),
		'update_item'                	=> __( 'Update '.'Staff Role'.'',                     	'churchamp-staff' ),
		'add_new_item'               	=> __( 'Add New '.'Staff Role'.'',                    	'churchamp-staff' ),
		'new_item_name'             	=> __( 'New '.'Staff Role'.' Name',                	'churchamp-staff' ),
		'separate_items_with_commas' 	=> __( 'Separate '.'Staff Roles'.' with Commas',      	'churchamp-staff' ),
		'add_or_remove_items'        	=> __( 'Add or Remove '.'Staff Roles'.'',             	'churchamp-staff' ),
		'choose_from_most_used'      	=> __( 'Choose from the Most Used '.'Staff Roles'.'',	'churchamp-staff' ),
	);
	/* only 2 caps are needed: 'manage_staff' and 'edit_staff'. */
	$capabilities = array(
		'manage_terms' 			=> 'manage_staff',
		'edit_terms'   			=> 'manage_staff',
		'delete_terms' 			=> 'manage_staff',
		'assign_terms' 			=> 'edit_staff',
	);
	$rewrite = array(
		'slug'         			=> 'staff/role',
		'with_front'   			=> false,
		'hierarchical' 			=> false,
		'ep_mask'      			=> EP_NONE,
	);
	$args = array(
		'public'            		=> true,
		'show_ui'           		=> true,
		'show_in_nav_menus' 		=> true,
		'show_tagcloud'     		=> false,
		'show_admin_column' 		=> true,
		'hierarchical'      		=> true, // if set to true, the taxonomy will function like categories, and false = functionality like tags
		'query_var'         		=> 'staffrole',
		'capabilities' 			=> $capabilities,
		'rewrite' 				=> $rewrite,
		'labels' 					=> $labels,
	);

	/* register the 'staffrole' taxonomy. */
	register_taxonomy( 'staffrole', array( 'staff' ), $args );
}