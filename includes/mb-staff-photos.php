<?php
/**
 * Meta Boxes ( Register Field Groups for Staff CPT )
 *
 * @package  		ChurchAmp_Staff
 * @subpackage  	Includes
 * @version  		5.0.0
 * @since   		1.0.0
 * @author  		Endeavr Media <support@endeavr.com>
 * @copyright  	Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link   		http://churchamp.com/plugins/staff
 * @license  		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * The Meta Boxes require the Advanced Custom Fields plugin be activated.
 *
 * The register_field_group function accepts 1 array which holds the relevant data to register a field group
 * You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
 * This code must run every time the functions.php file is read
 */

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => '50e92735c6d53',
		'title' => 'Staff Photos',
		'fields' =>
		array (
			0 =>
			array (
				'key' => '_endvr_staff_photo_thumb',
				'label' => 'Photo (Thumbnail Size)',
				'name' => '_endvr_staff_photo_thumb',
				'type' => 'image',
				'order_no' => 0,
				'instructions' => 'This photo will be used on the staff directory index page. It should be a headshot at least 200px wide.',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
			),
			1 =>
			array (
				'key' => '_endvr_staff_photo_full',
				'label' => 'Photo (Full Size)',
				'name' => '_endvr_staff_photo_full',
				'type' => 'image',
				'order_no' => 1,
				'instructions' => 'This photo will be used on this staff person\'s bio page. It should be a large image at least 600px wide.',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'save_format' => 'url',
				'preview_size' => 'large',
			),
		),
		'location' =>
		array (
			'rules' =>
			array (
				0 =>
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'staff',
					'order_no' => 2,
				),
			),
			'allorany' => 'all',
		),
		'options' =>
		array (
			'position' => 'advanced',
			'layout' => 'default',
			'hide_on_screen' =>
			array (
			),
		),
		'menu_order' => 2,
	));
}